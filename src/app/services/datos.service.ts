import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { map } from 'rxjs/operators';

interface Paciente {
  nombre: string;
  oxigenacion: string;
  presion: string;
  temperatura: number;
  contrasena: string;
}

@Injectable({
  providedIn: 'root',
})
export class DatosService {
  paciente: Paciente = {
    nombre: '',
    oxigenacion: '',
    presion: '',
    temperatura: 0,
    contrasena: '',
  };

  get Nombre() {
    return this.paciente.nombre;
  }

  constructor(private db: AngularFireDatabase) {}

  getUsuario(nombre: string = 'nulo', contrasena: string = '') {
    return this.db
      .object(nombre)
      .valueChanges()
      .pipe(
        map((resp: any) => {
          if (!resp) {
            return {
              msg: 'Usuario no encontrado',
            };
          }
          if (resp.contrasena == contrasena) {
            return {
              msg: 'Correcto',
            };
          }
          return {
            msg: 'Contraseña incorrecta',
          };
        })
      );
  }

  getDatoPaciente(nombre: string) {
    return this.db
      .object(nombre)
      .valueChanges()
      .pipe(
        map((resp: any) => {
          let oxiData = 'baja';
          let presData = 'baja';

          if (resp) {
            if (resp.Oxigenacion.Alta == 1) {
              oxiData = 'alta';
            } else if (resp.Oxigenacion.Media == 1) {
              oxiData = 'media';
            } else if (resp.Oxigenacion.Baja == 1) {
              oxiData = 'baja';
            }
          }

          if (resp) {
            if (resp.Presion.Alta == 1) {
              presData = 'alta';
            } else if (resp.Presion.Media == 1) {
              presData = 'media';
            } else if (resp.Presion.Baja == 1) {
              presData = 'baja';
            }
          }

          return {
            oxigenacion: oxiData,
            presion: presData,
            temperatura: resp.Temperatura,
            nombre: resp.nombre,
          };
        })
      );
  }

  setUsuario(nombre: string, contrasena: string) {
    this.paciente.nombre = nombre;
    this.paciente.contrasena = contrasena;
  }

  setOxigenacion(dato: string) {
    // console.log('oxi', dato);
    if (dato == 'alta') {
      this.db
        .object(this.paciente.nombre)
        .update({ Oxigenacion: { Alta: 1, Media: 0, Baja: 0 } });
    } else if (dato == 'media') {
      this.db
        .object(this.paciente.nombre)
        .update({ Oxigenacion: { Alta: 0, Media: 1, Baja: 0 } });
    } else if (dato == 'baja') {
      this.db
        .object(this.paciente.nombre)
        .update({ Oxigenacion: { Alta: 0, Media: 0, Baja: 1 } });
    }
  }

  setPresion(dato: string) {
    // console.log('pres', dato);
    if (dato == 'alta') {
      this.db
        .object(this.paciente.nombre)
        .update({ Presion: { Alta: 1, Media: 0, Baja: 0 } });
    } else if (dato == 'media') {
      this.db
        .object(this.paciente.nombre)
        .update({ Presion: { Alta: 0, Media: 1, Baja: 0 } });
    } else if (dato == 'baja') {
      this.db
        .object(this.paciente.nombre)
        .update({ Presion: { Alta: 0, Media: 0, Baja: 1 } });
    }
  }
}
