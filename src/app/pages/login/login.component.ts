import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatosService } from 'src/app/services/datos.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  usuario: string = '';
  contrasena: string = '';
  msg: string = '';
  correcto: boolean = true;

  constructor(private datosService: DatosService, private router: Router) {}

  ngOnInit(): void {}

  login() {
    if (this.usuario && this.contrasena) {
      this.datosService
        .getUsuario(this.usuario, this.contrasena)
        .subscribe((resp) => {
          if (resp.msg == 'Correcto') {
            this.correcto = true;
            // console.log(resp);
            this.datosService.setUsuario(this.usuario, this.contrasena);
            this.router.navigate(['pacientes']);
          } else {
            this.correcto = false;
            this.msg = resp.msg;
          }
        });
    }
  }
}
