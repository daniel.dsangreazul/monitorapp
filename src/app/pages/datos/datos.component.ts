import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatosService } from 'src/app/services/datos.service';

@Component({
  selector: 'app-datos',
  templateUrl: './datos.component.html',
  styleUrls: ['./datos.component.css'],
})
export class DatosComponent implements OnInit {
  paciente: string = '';
  oxigenacion: string = '';
  presion: string = '';
  temperatura: number = 0;

  constructor(private datosServices: DatosService, private router: Router) {}

  ngOnInit(): void {
    this.paciente = this.datosServices.Nombre;
    if (!this.paciente) {
      this.router.navigate(['login']);
    } else {
      this.datosServices.getDatoPaciente(this.paciente).subscribe((datos) => {
        // console.log(datos);
        this.paciente = datos.nombre;
        this.oxigenacion = datos.oxigenacion;
        this.presion = datos.presion;
        this.temperatura = datos.temperatura;
      });
    }
  }

  salir() {
    this.router.navigate(['login']);
  }

  oxiAlta() {
    this.datosServices.setOxigenacion('alta');
  }
  oxiMedia() {
    this.datosServices.setOxigenacion('media');
  }
  oxiBaja() {
    this.datosServices.setOxigenacion('baja');
  }

  presAlta() {
    this.datosServices.setPresion('alta');
  }
  presMedia() {
    this.datosServices.setPresion('media');
  }
  presBaja() {
    this.datosServices.setPresion('baja');
  }
}
