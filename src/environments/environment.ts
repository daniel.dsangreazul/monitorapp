// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBfqpRTulVgcjCp-evP46mHUOtwCbcjzB4",
    authDomain: "top2-86a8f.firebaseapp.com",
    databaseURL: "https://top2-86a8f-default-rtdb.firebaseio.com",
    projectId: "top2-86a8f",
    storageBucket: "top2-86a8f.appspot.com",
    messagingSenderId: "977045839177",
    appId: "1:977045839177:web:cef645c24ff350ca6bce98"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
